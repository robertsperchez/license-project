#include <SoftwareSerial.h>
#include <TinyGPS++.h>
//SoftwareSerial BTserial(8, 9); // RX | TX

SoftwareSerial GPSserial(11, 10); // RX | TX
TinyGPSPlus gps;

void setup() {
  Serial.begin(9600);
  GPSserial.begin(9600);
  Serial.println("Connect BT device");
}

void loop(){
   // Feed any data from bluetooth to Terminal.
  //if (BTserial.available())
    //Serial.write(BTserial.read());
 
  // Feed all data from termial to bluetooth
  if (Serial.available())
    {
      char c = Serial.read();
      Serial.println(c);
    }

  while (GPSserial.available() > 0){
    // get the byte data from the GPS
    gps.encode(GPSserial.read());
  }

  Serial.print("LAT=");  Serial.println(gps.location.lat(), 6);
  Serial.print("LONG="); Serial.println(gps.location.lng(), 6);
  Serial.print("ALT=");  Serial.println(gps.altitude.meters());
}

 /*
const long baudRate = 38400; 
char c=' ';
boolean NL = true;
 
void setup() 
{
    Serial.begin(9600);
    Serial.print("Sketch:   ");   Serial.println(__FILE__);
    Serial.print("Uploaded: ");   Serial.println(__DATE__);
    Serial.println(" ");
 
    BTserial.begin(baudRate);  
    Serial.print("BTserial started at "); Serial.println(baudRate);
    Serial.println(" ");
}
 
void loop()
{
 
    // Read from the Bluetooth module and send to the Arduino Serial Monitor
    if (BTserial.available())
    {
        c = BTserial.read();
        Serial.write(c);
    }
 
 
    // Read from the Serial Monitor and send to the Bluetooth module
    if (Serial.available())
    {
        c = Serial.read();
        BTserial.write(c);   
 
        // Echo the user input to the main window. The ">" character indicates the user entered text.
        if (NL) { Serial.print(">");  NL = false; }
        Serial.write(c);
        if (c==10) { NL = true; }
    }
 
}
*/
