#include <SoftwareSerial.h>
#include <Servo.h> 
#include <NewPing.h>

#define trig_ultrasonic A1 //analog input 1
#define echo_ultrasonic A2 //analog input 2
#define maximum_distance 200

Servo servoMotor;
SoftwareSerial BTserial(8, 9); // RX | TX
NewPing sonar(trig_ultrasonic, echo_ultrasonic, maximum_distance); //sensor function
int motorRightIN1 = 7;
int motorRightIN2 = 6;
int motorLeftIN3 = 5;
int motorLeftIN4 = 4;
char command;
int distance = 100; // sensor read distance


void setup() {
  pinMode(motorRightIN1, OUTPUT);
  pinMode(motorRightIN2, OUTPUT);
  pinMode(motorLeftIN3, OUTPUT);
  pinMode(motorLeftIN4, OUTPUT);
 
  servoMotor.attach(3);
  servoMotor.write(90);
  delay(2000);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);

  BTserial.begin(9600);
  Serial.begin(9600);
  delay(15);
}


void loop() {
  while(BTserial.available()) {
    command = BTserial.read();
    Serial.println(command);
    if(distance <= 20) {
      int distanceRight = 0;
      int distanceLeft = 0;
      avoidObstacles(distanceRight, distanceLeft, distance);
    }
    switch(command){
      case 'W':
        goForward();
        delay(50);
        stopMotor();
        break;
      case 'S':
        goBackward();
        delay(50);
        stopMotor();
        break;
      case 'A':
        goLeft();
        delay(50);
        stopMotor();
        break;
      case 'D':
        goRight();
        delay(50);
        stopMotor();
        break;
      case 'I':
        goForward();
        delay(500);
        stopMotor();
        break;
      case 'K':
        goBackward();
        delay(500);
        stopMotor();
        break;
      case 'J':
        goLeft();
        delay(300);
        stopMotor();
        break;
      case 'L':
        goRight();
        delay(300);
        stopMotor();
        break;
      default:
        stopMotor();
        break;
    }
    distance = readPing();
    Serial.println(distance);
  }
}

void avoidObstacles(int distanceRight, int distanceLeft, int distance){
  stopMotor();
  delay(300);
  goBackward();
  delay(400);
  stopMotor();
  delay(300);
  distanceRight = lookRight();
  delay(300);
  distanceLeft = lookLeft();
  delay(300);
  if (distance >= distanceLeft){
    goRight();
    delay(300);
    stopMotor();
  }
  else{
    goLeft();
    delay(300);
    stopMotor();
  }
}

void goForward(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, LOW);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, LOW);
}

void goBackward(){
  digitalWrite(motorRightIN1, LOW);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, LOW);
  digitalWrite(motorLeftIN4, HIGH);
}

void goLeft(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, LOW);
  digitalWrite(motorLeftIN3, LOW);
  digitalWrite(motorLeftIN4, HIGH);
}

void goRight(){
  digitalWrite(motorRightIN1, LOW);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, LOW);
}

void stopMotor(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, HIGH);
}

int lookRight(){  
  servoMotor.write(30);
  delay(500);
  int distance = readPing();
  delay(100);
  servoMotor.write(90);
  return distance;
}

int lookLeft(){
  servoMotor.write(150);
  delay(500);
  int distance = readPing();
  delay(100);
  servoMotor.write(90);
  return distance;
}

int readPing(){
  //delay(70);
  int cm = sonar.ping_cm();
  if (cm == 0){
    cm = 250;
  }
  return cm;
}
