#include <SoftwareSerial.h>

SoftwareSerial BTserial(8, 9); // RX | TX
int motorRightIN1 = 7;
int motorRightIN2 = 6;
int motorLeftIN3 = 5;
int motorLeftIN4 = 4;
char command;

void setup() {
  pinMode(motorRightIN1, OUTPUT);
  pinMode(motorRightIN2, OUTPUT);
  pinMode(motorLeftIN3, OUTPUT);
  pinMode(motorLeftIN4, OUTPUT);
  BTserial.begin(38400);
  Serial.begin(9600);
}

void loop() {
  
  while(BTserial.available() > 0) {
    command = BTserial.read();
    Serial.println(command);
    switch(command){
      case 'W':
        goForward();
        delay(50);
        stopMotor();
        break;
      case 'S':
        goBackward();
        delay(50);
        stopMotor();
        break;
      case 'A':
        goLeft();
        delay(50);
        stopMotor();
        break;
      case 'D':
        goRight();
        delay(50);
        stopMotor();
        break;
      default:
        stopMotor();
        break;
    }
  }
}

void goForward(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, LOW);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, LOW);
}

void goBackward(){
  digitalWrite(motorRightIN1, LOW);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, LOW);
  digitalWrite(motorLeftIN4, HIGH);
}

void goLeft(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, LOW);
  digitalWrite(motorLeftIN3, LOW);
  digitalWrite(motorLeftIN4, HIGH);
}

void goRight(){
  digitalWrite(motorRightIN1, LOW);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, LOW);
}

void stopMotor(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, HIGH);
}
