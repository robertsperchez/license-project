#include <MechaQMC5883.h>
#include <SoftwareSerial.h>
#include <PWMServo.h> 
#include <NewPing.h>
#include <Wire.h>
#include <TinyGPS++.h>

//Ultrasonic sensor analog inputs
#define trig_ultrasonic A1 
#define echo_ultrasonic A2
//Maximum read distance of the sensor 
#define maximum_distance 200
//Magnetic declination degrees (it's +6 degrees in Brasov : https://www.magnetic-declination.com/)
#define declination_angle 6

//DC Motors control pins
int motorRightIN1 = 7;
int motorRightIN2 = 6;
int motorLeftIN3 = 5;
int motorLeftIN4 = 4;
//Command received by the Bluetooth to control the Cart
char command;
//String that saves the coordinates received from the smartphone
String locationString;
//For testing purposes. Allows entering an if statement that will print data for the GPS feature
bool coordinatesAreRead = false;
//Phone and Arduino coordinates
double phoneLatitude, phoneLongitude;
double arduinoLatitude, arduinoLongitude;
//Variable to store de distance from the Ultrasonic sensor
int distance = 100; 

//Sensors
TinyGPSPlus arduinoGPS;
MechaQMC5883 compass;
SoftwareSerial GPSserial(11, 10);
NewPing sonar(trig_ultrasonic, echo_ultrasonic, maximum_distance);
PWMServo servo;

/**
 * Function that will run only once, at the beginning of the program. 
 * Initializes the wheels, GPS, compass and serial (which is used for bluetooth communication). 
 * Initializes the servomotor, moves it to the starting position, then reads the distance from the Ultrasonic sensor.
 */
void setup() {
  pinMode(motorRightIN1, OUTPUT);
  pinMode(motorRightIN2, OUTPUT);
  pinMode(motorLeftIN3, OUTPUT);
  pinMode(motorLeftIN4, OUTPUT);

  servo.attach(9);
  servo.write(90);
  
  delay(2000);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);

  Wire.begin();
  GPSserial.begin(9600);
  Serial.begin(9600);
  compass.init();

  delay(15);
}

/**
 * Function that runs in a loop while the Arduino receives power. 
 * It's the main function, where all of the control commands are called.
 */
void loop() {
  //Reads the command character received from the smartphone via Bluetooth
  if(Serial.available()) {
      command = Serial.read();
      Serial.println(command);
  }

  //If it detects an object in front of the Cart at 20cm or less, it will stop and avoid it
  if(distance <= 20) {
      int distanceRight = 0;
      int distanceLeft = 0;
      avoidObstacles(distanceRight, distanceLeft, distance);
  }

  //Forward command from the remote control mode
  if(command == 'W'){
      goForward();
      delay(50);
      stopMotor();
  }

  //Backward command from the remote control mode
  if(command == 'S'){
      goBackward();
      delay(50);
      stopMotor();
  }

  //Left command from the remote control mode
  if(command == 'A'){
      goLeft();
      delay(50);
      stopMotor();
  }

  //Right command from the remote control mode
  if(command == 'D'){
      goRight();
      delay(50);
      stopMotor();
  }

  //Forward command from the voice control mode
  if(command == 'I'){
      goForward();
      delay(1000);
      stopMotor();
  }

  //Backward command from the voice control mode
  if(command == 'K'){
      goBackward();
      delay(1000);
      stopMotor();
  }

  //Left command from the voice control mode
  if(command == 'J'){
      goLeft();
      delay(500);
      stopMotor();
  }

  //Right command from the voice control mode
  if(command == 'L'){
      goRight();
      delay(500);
      stopMotor();
  }

  //GPS follow mode command
  if(command == 'X'){
      //Reads the coordinates from the phone, transforms the string into doubles
      readPhoneCoordinates();
      stringToCoordinates();
      delay(10);
      //Reads the coordinates from the GPS sensor
      readArduinoCoordinates();
      //Reads the compass heading, makes it turn in the phone's direction
      readCompass(bearingBetweenPoints(arduinoLatitude, arduinoLongitude, phoneLatitude, phoneLongitude));
      //Drives forward to the user
      driveToUser();
      coordinatesAreRead = true;
  }

  Serial.println("Location string:");
  Serial.print(locationString);

  //For testing purposes. It prints the coordinates, bearing and distance between points
  if(coordinatesAreRead == true){
      Serial.println(locationString);
      Serial.println("PhoneLatitude= ");
      Serial.println(phoneLatitude, 7);
      Serial.println("PhoneLongitude= ");
      Serial.println(phoneLongitude, 7);
      Serial.print("ArdLatitude= "); 
      Serial.print(arduinoLatitude, 7);
      Serial.print("ArdLongitude= "); 
      Serial.println(arduinoLongitude, 7);
      Serial.print("Bearing:");
      Serial.println(bearingBetweenPoints(arduinoLatitude, arduinoLongitude, phoneLatitude, phoneLongitude));
      Serial.print("Distance:");
      Serial.println(distanceBetweenPoints(arduinoLatitude, arduinoLongitude, phoneLatitude, phoneLongitude));
      coordinatesAreRead = false;
  }

  //Reads the distance from the sensor again
  distance = readPing();
  command = "";    
}

/**
 * It goes forward to the user until it is half a meter close to him, then stops
 */
void driveToUser(){
  distance = readPing();
  while(distance > 50){
    goForward();
    delay(50);
    distance = readPing();
  }
  stopMotor();
}

/**
 * Gets the x,y,z variables from the compass, then calculates the azimuth/bearing (accounting for the magnetic declination) in which the Cart is facing.
 * After that, the cart turns left until it is facing the user's direction.
 * @param degr - the bearing of the user's location compared to the Arduino
 */
void readCompass(int degr){  
  int x,y,z;
  int azimuth;
  compass.read(&x,&y,&z);
  azimuth = compass.azimuth(&y,&x) - declination_angle;
  if(azimuth < 0)
    azimuth += 360;
  Serial.println(azimuth);
  while(azimuth - declination_angle < degr - 10 || azimuth - declination_angle > degr + 10){
    goLeft();
    delay(50);
    compass.read(&x,&y,&z);
    azimuth = compass.azimuth(&y,&x) - declination_angle;
    if(azimuth < 0)
      azimuth += 360;
    Serial.println(azimuth);
  }
  stopMotor();
}

/**
 * Reads the coordinates from the GPS NEO-6M sensor, and it updates them if they have changed
 */
void readArduinoCoordinates(){
  while(GPSserial.available()){
    arduinoGPS.encode(GPSserial.read());
    if(arduinoGPS.location.isUpdated()){
        arduinoLatitude = arduinoGPS.location.lat();
        arduinoLongitude = arduinoGPS.location.lng();
    }
  }
}

/**
 * Reads the coordinates stream sent by the Android app
 */
void readPhoneCoordinates(){
  while(Serial.available()) {
    char coordChar = Serial.read();
    locationString += coordChar;
  }
}

/**
 * Transforms the location string into two doubles, so that the coordinates can be compared to the Arduino ones
 */
void stringToCoordinates(){
  String phoneLatitudeString = locationString.substring(0,10);
  String phoneLongitudeString = locationString.substring(10,20);
  locationString = "";
  phoneLatitude = phoneLatitudeString.toDouble();
  phoneLongitude = phoneLongitudeString.toDouble();
}

/**
 * Calculates the bearing between two coordinates, so that the Cart will know in which direction the user is
 */
double bearingBetweenPoints(double ardLat, double ardLong, double phoneLat, double phoneLong){
  double y = sin(phoneLong - ardLong) * cos(phoneLat);
  double x = cos(ardLat) * sin(phoneLat) - sin(ardLat) * cos(phoneLat) * cos(phoneLong - ardLong);
  double bearingRadians = atan2(y, x);
  Serial.println(bearingRadians);
  double bearingDegrees = bearingRadians * RAD_TO_DEG;
  if(bearingDegrees < 0)
    bearingDegrees += 360;
  return bearingDegrees;
}

/**
 * Calculates the distance between two coordinates in meters, so the Cart will know when it's close to the user
 * *Currently not in use, as the sensors are not accurate enough
 */
double distanceBetweenPoints(double ardLat, double ardLong, double phoneLat, double phoneLong){
  double earthRadius = 6371000;
  double radArdLat = ardLat * DEG_TO_RAD;
  double radPhoneLat = phoneLat * DEG_TO_RAD;
  double deltaLat = (phoneLat - ardLat) * DEG_TO_RAD;
  double deltaLong = (phoneLong - ardLong) * DEG_TO_RAD;
  double squareHalf = sin(deltaLat/2) * sin(deltaLat/2) + cos(radArdLat) * cos(radPhoneLat) * sin(deltaLong/2) * sin(deltaLong/2);
  double angularDistance = 2 * atan2(sqrt(squareHalf), sqrt(1-squareHalf));
  return earthRadius * angularDistance;
}

/**
 * Makes the cart go forward
 */
void goForward(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, LOW);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, LOW);
}

/**
 * Makes the cart go backward
 */
void goBackward(){
  digitalWrite(motorRightIN1, LOW);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, LOW);
  digitalWrite(motorLeftIN4, HIGH);
}

/**
 * Makes the cart turn left
 */
void goLeft(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, LOW);
  digitalWrite(motorLeftIN3, LOW);
  digitalWrite(motorLeftIN4, HIGH);
}

/**
 * Makes the cart turn right
 */
void goRight(){
  digitalWrite(motorRightIN1, LOW);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, LOW);
}

/**
 * Stops the motors
 */
void stopMotor(){
  digitalWrite(motorRightIN1, HIGH);
  digitalWrite(motorRightIN2, HIGH);
  digitalWrite(motorLeftIN3, HIGH);
  digitalWrite(motorLeftIN4, HIGH);
}

/**
 * Obstacle avoiding feature. When called, it goes backwards, reads the distance in both directions, and chooses the best direction to turn to
 */
void avoidObstacles(int distanceRight, int distanceLeft, int distance){
  stopMotor();
  delay(300);
  goBackward();
  delay(400);
  stopMotor();
  delay(300);
  distanceRight = lookRight();
  delay(300);
  distanceLeft = lookLeft();
  delay(300);
  if (distanceRight >= distanceLeft){
    goRight();
    delay(300);
    stopMotor();
  }
  else{
    goLeft();
    delay(300);
    stopMotor();
  }
}

/**
 * Makes the Ultrasonic sensor turn right using the servomotor, and reads the distance in that direction
 */
int lookRight(){  
  servo.write(30);
  delay(500);
  int distance = readPing();
  delay(100);
  servo.write(90);
  return distance;
}

/**
 * Makes the Ultrasonic sensor turn left using the servomotor, and reads the distance in that direction
 */
int lookLeft(){
  servo.write(150);
  delay(500);
  int distance = readPing();
  delay(100);
  servo.write(90);
  return distance;
}

/**
 * Function to read the distance from the Ultrasonic sensor
 */
int readPing(){
  int cm = sonar.ping_cm();
  if (cm == 0){
    cm = 250;
  }
  return cm;
}
