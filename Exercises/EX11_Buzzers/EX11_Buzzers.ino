/*Eleventh Arduino tutorial, about:
 * - potentiometer
 * - buzzers (active and passive)
 * Created a program that takes the value from the potentiometer, 
 * and when a certain value is passed, it rings the buzzer.
 */
 
int buzzer = 5;
int potentiometer = A3;
int potValue;

void setup() {
  pinMode(buzzer, OUTPUT);
  pinMode(potentiometer, INPUT);
  Serial.begin(9600);
}

void loop() {
  potValue = analogRead(potentiometer);
  Serial.println(potValue);
  while(potValue>1000){
    analogWrite(buzzer, 100);
    potValue = analogRead(potentiometer);
    Serial.println(potValue);
  }
  analogWrite(buzzer, 0);
}
