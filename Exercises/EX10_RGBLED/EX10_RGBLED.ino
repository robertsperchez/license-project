/*Tenth Arduino exercise, about:
 * - RGB LED
 * Created an exercise that takes 3 values and outputs them on the LED.
 */

int redPin = 11;
int greenPin = 10;
int bluePin = 9;
int redValue = 0;
int greenValue = 0;
int blueValue = 0;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  Serial.println("Write the RGB values you want: ");
  while(Serial.available()==0){
  }

  redValue = Serial.parseInt();
  greenValue = Serial.parseInt();
  blueValue = Serial.parseInt();
  
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
