int blueLED = 10;
int shortFlash = 150;
int longFlash = 500;
int shortPause = 700;
int longPause = 2000;

void setup() {
  pinMode(blueLED, OUTPUT);
}

void loop() {
  morseR();
  delay(shortPause);
  morseO();
  delay(shortPause);
  morseB();
  delay(shortPause);
  morseE();
  delay(shortPause);
  morseR();
  delay(shortPause);
  morseT();

  delay(longPause);
}

void morseR(){
  digitalWrite(blueLED, HIGH);
  delay(shortFlash);
  digitalWrite(blueLED, LOW);
  delay(shortFlash);

  digitalWrite(blueLED, HIGH);
  delay(longFlash);
  digitalWrite(blueLED, LOW);
  delay(longFlash);

  digitalWrite(blueLED, HIGH);
  delay(shortFlash);
  digitalWrite(blueLED, LOW);
  delay(shortFlash);
}

void morseO(){
  for(int index = 0; index < 3; index++){
    digitalWrite(blueLED, HIGH);
    delay(longFlash);
    digitalWrite(blueLED, LOW);
    delay(longFlash);
  }
}

void morseB(){
  digitalWrite(blueLED, HIGH);
  delay(longFlash);
  digitalWrite(blueLED, LOW);
  delay(longFlash);
  for(int index = 0; index < 3; index++){
    digitalWrite(blueLED, HIGH);
    delay(shortFlash);
    digitalWrite(blueLED, LOW);
    delay(shortFlash);
  }
}

void morseE(){
  digitalWrite(blueLED, HIGH);
  delay(shortFlash);
  digitalWrite(blueLED, LOW);
  delay(shortFlash);
}

void morseT(){
  digitalWrite(blueLED, HIGH);
  delay(longFlash);
  digitalWrite(blueLED, LOW);
  delay(longFlash);
}

/*Third Arduino tutorial, about:
 * - Variables
 * Did a morse code exercise. The program signals my name in morse code.
 */
