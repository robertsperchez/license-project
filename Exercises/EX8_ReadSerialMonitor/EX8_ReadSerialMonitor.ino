/*Eighth Arduino exercise, about:
 * - Reading from serial monitors
 * Exercise about getting a number from a Serial monitor and blinking an LED as many times
 */
String numberRequest = "How many LED blinks do you want: ";
int numberOfBlinks;
int redLED = 12;
int blinkDelay = 250;

void setup() {
  pinMode(redLED, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  Serial.println(numberRequest);
  while(Serial.available()==0) { //this loop waits until input
    
  }
  numberOfBlinks = Serial.parseInt();
  
  for(int index = 0; index < numberOfBlinks; index++) {
    digitalWrite(redLED, HIGH);
    delay(blinkDelay);
    digitalWrite(redLED, LOW);
    delay(blinkDelay);
  }
 
}
