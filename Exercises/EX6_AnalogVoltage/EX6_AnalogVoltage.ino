/* Sixth Arduino tutorial, about:
 *  - resistors
 *  - voltages
 *  - Ohm's Law
 *  - analog input
 *  Exercise about reading the voltages off the breadboard.
 */

int readPin = A3;
double voltage = 0;
int readVal = 0;
int printDelay = 500;

void setup() {
  pinMode(readPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  readVal = analogRead(readPin);
  voltage = (5./1023.)*readVal;
  Serial.println(voltage);
  delay(printDelay);
}
