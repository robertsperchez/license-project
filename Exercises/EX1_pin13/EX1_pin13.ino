void setup() {
  pinMode(13, OUTPUT);
}

void loop() {
  digitalWrite(13, HIGH);
  delay(50);
  digitalWrite(13, LOW);
  delay(50);
}

/*First Arduino program, about:
 * - Working with pin13(LED)
 * - The basics(loop and setup)
 */
