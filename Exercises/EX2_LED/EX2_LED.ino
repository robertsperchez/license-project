void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
}

void loop() {
  for(int index = 0; index < 5; index++){
    digitalWrite(2, HIGH);
    delay(200);
    digitalWrite(2, LOW);
    delay(200);
  }

  for(int index = 0; index < 10; index++){
    digitalWrite(3, HIGH);
    delay(200);
    digitalWrite(3, LOW);
    delay(200);
  }

  for(int index = 0; index < 15; index++){
    digitalWrite(4, HIGH);
    delay(200);
    digitalWrite(4, LOW);
    delay(200);
  }
}

/*Second Arduino tutorial, about:
 * - How to work with a breadboard
 * - Resistor and Ground
 * - LED Exercise
 * Exercise was about working with 3 LED's that have different pins
 */
