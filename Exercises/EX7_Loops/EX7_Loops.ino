int redLED = 9;
int yellowLED = 6;
int shortDelay = 300;
int longDelay = 1000;

void setup() {
  pinMode(redLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
}

void loop() {
  for(int index = 0; index < 3; index++){
    digitalWrite(yellowLED, HIGH);
    delay(shortDelay);
    digitalWrite(yellowLED, LOW);
    delay(shortDelay);
  }
  
  for(int index = 0; index < 5; index++){
    digitalWrite(redLED, HIGH);
    delay(shortDelay);
    digitalWrite(redLED, LOW);
    delay(shortDelay);
  }

  delay(longDelay);
}
