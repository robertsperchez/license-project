/*Arduino tutorial number 6, about:
 * - Binary numbers
 * - A bit more about breadboard and connectivity
 * As an exercise, created this binary calculator that outputs in LEDs.
 */
 
int ledOne = 2;
int ledTwo = 3;
int ledThree = 4;
int ledFour = 5;

void setup() {
  pinMode(ledOne, OUTPUT);
  pinMode(ledTwo, OUTPUT);
  pinMode(ledThree, OUTPUT);
  pinMode(ledFour, OUTPUT);
}

void loop() {
  decimalToBinary(1);
  delay(500);
  decimalToBinary(7);
  delay(500);
  decimalToBinary(10);
  delay(500);
  decimalToBinary(13);
  delay(2000);
}

void decimalToBinary(int number){
  int binaryNumber[4];
  int index = 0;
  
  while(number > 0){
    binaryNumber[index] = number%2;
    number/=2;
    index++;
  }
  
  if(binaryNumber[3] == 1)
    digitalWrite(ledOne, HIGH);
  else
    digitalWrite(ledOne, LOW);

  if(binaryNumber[2] == 1)
    digitalWrite(ledTwo, HIGH);
  else
    digitalWrite(ledTwo, LOW);

  if(binaryNumber[1] == 1)
    digitalWrite(ledThree, HIGH);
  else
    digitalWrite(ledThree, LOW);

  if(binaryNumber[0] == 1)
    digitalWrite(ledFour, HIGH);
  else
    digitalWrite(ledFour, LOW);
}
