/*Arduino Tutorial 6, about:
 * - The analogWrite command
 * Created a pulsing LED that has different intensities.
 */

int redLED = 9;

void setup() {
  pinMode(redLED, OUTPUT);
}

void loop() {
  for(int brightness = 0; brightness < 256; brightness++){
    analogWrite(redLED, brightness);
    delay(10);
    if(brightness == 255)
      break;
  }
  
  for(int brightness = 255; brightness >= 0; brightness--){
    analogWrite(redLED, brightness);
    delay(50);
    if(brightness == 0)
      break;
  }

  delay(1000);
}
