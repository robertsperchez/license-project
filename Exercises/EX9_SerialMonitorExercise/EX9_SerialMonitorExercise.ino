/* Ninth Arduino tutorial, about:
 * - Reading from serial monitors
 * Exercise about reading a char and then lighting the corresponding LED.
 */

int redLED = 12;
int greenLED = 11;
int blueLED = 2;
char option;
int blinkDelay = 1000;

void setup() {
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  Serial.begin(9600);

}

void loop() {
  Serial.println("Which LED do you want to light up?");
  while(Serial.available()==0) {
    
  }
  option = Serial.read();
  
  switch(option) {
    case 'r':
      digitalWrite(redLED, HIGH);
      delay(blinkDelay);
      digitalWrite(redLED, LOW);
      delay(blinkDelay);
      break;
    case 'g':
      digitalWrite(greenLED, HIGH);
      delay(blinkDelay);
      digitalWrite(greenLED, LOW);
      delay(blinkDelay);
      break;
    case 'b':
      digitalWrite(blueLED, HIGH);
      delay(blinkDelay);
      digitalWrite(blueLED, LOW);
      delay(blinkDelay);
      break;
    default:
      Serial.println("Invalid option! Try r, g, b.");
      break;
  }
}
