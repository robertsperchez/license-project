package com.example.smartcart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OptionsMenu extends AppCompatActivity {

    //Widgets
    Button manualControlButton, gpsModeButton, voiceControlButton;
    //For sending the Bluetooth address to the next activities
    public static String EXTRA_ADDRESS = "device_address";
    //For getting the Bluetooth address from the previous activity
    String getExtraAddress = null;

    /**
     * Main class of this activity, where we call the widgets, and
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent newIntent = getIntent();
        //Receive the address of the bluetooth device from the previous activity
        getExtraAddress = newIntent.getStringExtra(DeviceList.EXTRA_ADDRESS);

        setContentView(R.layout.options_menu);

        //Calling the widgets using their component id
        manualControlButton = (Button) findViewById(R.id.manualControlButton);
        gpsModeButton = (Button) findViewById(R.id.gpsModeButton);
        voiceControlButton = (Button) findViewById(R.id.voiceControlButton);

        //Changes to the remote control activity
        manualControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OptionsMenu.this, RemoteControl.class);
                intent.putExtra(EXTRA_ADDRESS, getExtraAddress);
                startActivity(intent);
            }
        });

        //Changes to the GPS follow mode activity
        gpsModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OptionsMenu.this, GPSFollowMode.class);
                intent.putExtra(EXTRA_ADDRESS, getExtraAddress);
                startActivity(intent);
            }
        });

        //Changes to the voice control mode activity
        voiceControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OptionsMenu.this, VoiceControl.class);
                intent.putExtra(EXTRA_ADDRESS, getExtraAddress);
                startActivity(intent);
            }
        });
    }
}
