package com.example.smartcart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.io.IOException;
import java.util.UUID;

public class VoiceControl extends AppCompatActivity {

    //Widgets
    Button disconnectButton, getVoiceButton;
    //Bluetooth
    BluetoothAdapter bluetoothAdapter = null;
    BluetoothSocket bluetoothSocket = null;
    private boolean isConnected = false;
    String address = null;
    //SPP(Serial Port Profile) UUID - default for the Arduino Bluetooth piece
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    //Will be used for sending messages to the user
    private ProgressDialog progress;

    /**
     * Main function of the VoiceControl Activity. It initializes the widgets, gets information from the previous activity,
     * and sets commands for the button presses.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent newIntent = getIntent();
        //Receive the address of the bluetooth device
        address = newIntent.getStringExtra(OptionsMenu.EXTRA_ADDRESS);

        setContentView(R.layout.voice_control);

        //Calling the widgets using their component id
        disconnectButton = (Button) findViewById(R.id.disconnectButton);
        getVoiceButton = (Button) findViewById(R.id.getVoiceButton);
        new ConnectBluetooth().execute(); //Call the class to connect

        //Closes the bluetooth connection
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }
        });

        //Will start the voice listening activity if the button is pressed
        getVoiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent used for getting the voice command
                Intent voiceIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                //Uses the free form language model
                voiceIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                //Sets the language to english
                voiceIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
                startActivityForResult(voiceIntent, 1);
            }
        });
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == 1) {
                ArrayList<String> command = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if(command.get(0).equals("forward")){
                    commandForward();
                } else if (command.get(0).equals("backward")){
                    commandBackward();
                } else if (command.get(0).equals("left")){
                    commandLeft();
                } else if (command.get(0).equals("right")){
                    commandRight();
                } else {
                    Toast.makeText(getApplicationContext(), "Sorry, wrong command. Please try again.", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(getApplicationContext(), "Sorry, I didn't catch that! Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Sends a string to let the Arduino know to go forward when a button is pressed
     */
    private void commandForward() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("I".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sends a string to let the Arduino know to go backwards when a button is pressed
     */
    private void commandBackward() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("K".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sends a string to let the Arduino know to go left when a button is pressed
     */
    private void commandLeft() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("J".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sends a string to let the Arduino know to go right when a button is pressed
     */
    private void commandRight() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("L".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Disconnects the smartphone from the device it's connected to
     */
    private void disconnect() {
        //Checks if the bluetoothSocket is busy
        if (bluetoothSocket != null)
        {
            try {
                //Closes the connection
                bluetoothSocket.close();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
        //Returns to the previous layout
        finish();
    }

    /**
     * Class that helps us connect to the bluetooth device we want to
     */
    private class ConnectBluetooth extends AsyncTask<Void, Void, Void>
    {
        //Checks if our connection is successful
        private boolean connectSuccess = true;

        /**
         * Shows a connecting message to the user, for a more friendly UI
         */
        @Override
        protected void onPreExecute() {
            //Shows a progress dialog
            progress = ProgressDialog.show(VoiceControl.this, "Connecting...", "Please wait!");
        }

        /**
         * Does the connection in the background while the progress dialog is shown
         * @param devices
         * @return
         */
        @Override
        protected Void doInBackground(Void... devices) {
            try {
                if (bluetoothSocket == null || !isConnected) {
                    //Get the mobile bluetooth device
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    //Connects to the device's address and checks if it's available
                    BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
                    //creates a RFCOMM (SPP) connection
                    bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    //Start connection
                    bluetoothSocket.connect();
                }
            } catch (IOException e) {
                connectSuccess = false;
            }
            return null;
        }

        /**
         * After the connection was made in doInBackground, it checks if everything went fine
         * @param result
         */
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!connectSuccess) {
                Toast.makeText(getApplicationContext(), "Connection Failed. The Bluetooth device might not be turned on.", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_LONG).show();
                isConnected = true;
            }
            progress.dismiss();
        }
    }
}


