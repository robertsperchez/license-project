package com.example.smartcart;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;
import android.view.View;

import java.io.IOException;
import java.util.UUID;

import android.os.Bundle;

public class RemoteControl extends AppCompatActivity {

    //Widgets
    Button backButton, forwardButton, leftButton, rightButton, disconnectButton;
    //Bluetooth
    BluetoothAdapter bluetoothAdapter = null;
    BluetoothSocket bluetoothSocket = null;
    private boolean isConnected = false;
    //SPP(Serial Port Profile) UUID - default for the Arduino Bluetooth piece
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    String address = null;
    //Will be used for sending messages to the user
    private ProgressDialog progress;
    //Handler for continuously sending messages
    private Handler repeatUpdateHandler = new Handler();
    //Booleans
    private boolean isFwdPressed = false;
    private boolean isBwdPressed = false;
    private boolean isLeftPressed = false;
    private boolean isRightPressed = false;

    /**
     * Main function of the RemoteControl Activity. It initializes the widgets, gets information from the previous activity,
     * and sets commands for the button presses.
     * @param savedInstanceState
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent newIntent = getIntent();
        //Receive the address of the bluetooth device
        address = newIntent.getStringExtra(OptionsMenu.EXTRA_ADDRESS);

        setContentView(R.layout.remote_control);

        //Calling the widgets using their component id
        backButton = (Button) findViewById(R.id.backButton);
        forwardButton = (Button) findViewById(R.id.forwardButton);
        leftButton = (Button) findViewById(R.id.leftButton);
        rightButton = (Button) findViewById(R.id.rightButton);
        disconnectButton = (Button) findViewById(R.id.disconnectButton);

        //Call the class to connect the bluetooth
        new ConnectBluetooth().execute();

        //Small helper class to implement a repeat command, so that the car can go continuously
        class RepetitiveUpdater implements Runnable {
            /**
             * Checks which button is pressed and repeats a command
             */
            @Override
            public void run() {
                if (isFwdPressed) {
                    goForward();
                    repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), 50);
                }
                else if (isBwdPressed){
                    goBackward();
                    repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), 50);
                }
                else if (isLeftPressed){
                    goLeft();
                    repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), 50);
                }
                else if (isRightPressed) {
                    goRight();
                    repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), 50);
                }
            }
        }

        //Closes the bluetooth connection
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }
        });

        //Backward Button
        //Single click
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBackward();
            }
        });

        //Long press of the button (will repeatedly call the goBackward function while the button is pressed)
        backButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isBwdPressed = true;
                repeatUpdateHandler.post(new RepetitiveUpdater());
                return false;
            }
        });

        //Checks for when the press is lifted
        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)
                        && isBwdPressed ){
                    isBwdPressed = false;
                }
                return false;
            }
        });

        //Forward Button
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goForward();
            }
        });

        forwardButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isFwdPressed = true;
                repeatUpdateHandler.post(new RepetitiveUpdater());
                return false;
            }
        });

        forwardButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)
                        && isFwdPressed ){
                    isFwdPressed = false;
                }
                return false;
            }
        });

        //Left Button
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goLeft();
            }
        });

        leftButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isLeftPressed = true;
                repeatUpdateHandler.post(new RepetitiveUpdater());
                return false;
            }
        });

        leftButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)
                        && isLeftPressed ){
                    isLeftPressed = false;
                }
                return false;
            }
        });

        //Right Button
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goRight();
            }
        });

        rightButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isRightPressed = true;
                repeatUpdateHandler.post(new RepetitiveUpdater());
                return false;
            }
        });

        rightButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)
                        && isRightPressed ){
                    isRightPressed = false;
                }
                return false;
            }
        });
    }

    /**
     * Disconnects the smartphone from the device it's connected to
     */
    private void disconnect() {
        //Checks if the bluetoothSocket is busy
        if (bluetoothSocket != null)
        {
            try {
                //Closes the connection
                bluetoothSocket.close();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
        //Return to the previous layout
        finish();
    }

    /**
     * Sends a string to let the Arduino know to go forward when a button is pressed
     */
    private void goForward() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("W".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sends a string to let the Arduino know to go backwards when a button is pressed
     */
    private void goBackward() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("S".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sends a string to let the Arduino know to go left when a button is pressed
     */
    private void goLeft() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("A".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sends a string to let the Arduino know to go right when a button is pressed
     */
    private void goRight() {
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.getOutputStream().write("D".toString().getBytes());
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Class that helps us connect to the bluetooth device we want to
     */
    private class ConnectBluetooth extends AsyncTask<Void, Void, Void>  // UI thread
    {
        //Checks if our connection is successful
        private boolean connectSuccess = true;

        /**
         * Shows a connecting message to the user, for a more friendly UI
         */
        @Override
        protected void onPreExecute() {
            //Shows a progress dialog
            progress = ProgressDialog.show(RemoteControl.this, "Connecting...", "Please wait!");
        }

        /**
         * Does the connection in the background while the progress dialog is shown
         * @param devices
         * @return
         */
        @Override
        protected Void doInBackground(Void... devices)
        {
            try {
                if (bluetoothSocket == null || !isConnected) {
                    //Get the mobile bluetooth device
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    //Connects to the device's address and checks if it's available
                    BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
                    //Creates a RFCOMM (SPP) connection
                    bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    //Starts connection
                    bluetoothSocket.connect();
                }
            } catch (IOException e) {
                connectSuccess = false;
            }
            return null;
        }

        /**
         * After the connection was made in doInBackground, it checks if everything went fine
         * @param result
         */
        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);

            if (!connectSuccess) {
                Toast.makeText(getApplicationContext(), "Connection Failed. The Bluetooth device might not be turned on.", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_LONG).show();
                isConnected = true;
            }
            progress.dismiss();
        }
    }
}
