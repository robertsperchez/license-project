package com.example.smartcart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.UUID;

public class GPSFollowMode extends AppCompatActivity {

    //Widgets
    Button disconnectButton, startFollowButton;
    TextView latitudeTextView, longitudeTextView;
    //Bluetooth
    BluetoothAdapter bluetoothAdapter = null;
    BluetoothSocket bluetoothSocket = null;
    private boolean isConnected = false;
    String address = null;
    //SPP(Serial Port Profile) UUID - default for the Arduino Bluetooth piece
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    //Will be used for sending messages to the user
    private ProgressDialog progress;
    //Location
    int PERMISSION_ID = 44;
    FusedLocationProviderClient fusedLocationClient;
    double latitude, longitude;

    /**
     * Main function of the GPSFollowMode Activity. It initializes the widgets, gets information from the previous activity,
     * and sets commands for the button presses.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent newIntent = getIntent();
        //Receive the address of the bluetooth device
        address = newIntent.getStringExtra(OptionsMenu.EXTRA_ADDRESS);

        setContentView(R.layout.gps_follow_mode);

        //Calling the widgets using their component id
        disconnectButton = (Button) findViewById(R.id.disconnectButton);
        startFollowButton = (Button) findViewById(R.id.startFollowButton);
        latitudeTextView = (TextView) findViewById(R.id.latitudeTextView);
        longitudeTextView = (TextView) findViewById(R.id.longitudeTextView);

        //Call the class to connect
        new ConnectBluetooth().execute();

        //Closes the bluetooth connection
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }
        });

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getLastLocation();
        startFollowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLastLocation();
                if (bluetoothSocket != null) {
                    try {
                        bluetoothSocket.getOutputStream().write("X".toString().getBytes());
                        bluetoothSocket.getOutputStream().write(String.valueOf(latitude).getBytes());
                        bluetoothSocket.getOutputStream().write(String.valueOf(longitude).getBytes());
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkLocationPermissions()) {
            if (isLocationEnabled()) {
                fusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    latitudeTextView.setText("   Latitude: " + location.getLatitude() + "");
                                    longitudeTextView.setText("   Longitude: " + location.getLongitude() + "");
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestLocationPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(2000);
        locationRequest.setFastestInterval(1000);
        //locationRequest.setNumUpdates(1);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.requestLocationUpdates(
                locationRequest, locationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location lastLocation = locationResult.getLastLocation();
            latitudeTextView.setText("   Latitude: " + lastLocation.getLatitude() + "");
            longitudeTextView.setText("   Longitude: " + lastLocation.getLongitude() + "");
        }
    };

    /**
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    /**
     * Checks if the location is turned on on the device
     * @return
     */
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    /**
     * Requests the location permissions
     */
    private void requestLocationPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    /**
     * Checks if the device has the location permissions
     * @return
     */
    private boolean checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkLocationPermissions()) {
            getLastLocation();
        }
    }

    /**
     * Disconnects the smartphone from the device it's connected to
     */
    private void disconnect() {
        //Checks if the bluetoothSocket is busy
        if (bluetoothSocket != null)
        {
            try {
                //Closes the connection
                bluetoothSocket.close();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
            }
        }
        //Returns to the first layout
        finish();
    }

    /**
     * Class that helps us connect to the bluetooth device we want to
     */
    private class ConnectBluetooth extends AsyncTask<Void, Void, Void>
    {
        //Checks if our connection is successful
        private boolean connectSuccess = true;

        /**
         * Shows a connecting message to the user, for a more friendly UI
         */
        @Override
        protected void onPreExecute() {
            //Shows a progress dialog
            progress = ProgressDialog.show(GPSFollowMode.this, "Connecting...", "Please wait!");
        }

        /**
         * Does the connection in the background while the progress dialog is shown
         * @param devices
         * @return
         */
        @Override
        protected Void doInBackground(Void... devices) {
            try {
                if (bluetoothSocket == null || !isConnected) {
                    //Get the mobile bluetooth device
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    //Connects to the device's address and checks if it's available
                    BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
                    //Creates a RFCOMM (SPP) connection
                    bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    //Starts the connection
                    bluetoothSocket.connect();
                }
            } catch (IOException e) {
                connectSuccess = false;
            }
            return null;
        }

        /**
         * After the connection was made in doInBackground, it checks if everything went fine
         * @param result
         */
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!connectSuccess) {
                Toast.makeText(getApplicationContext(), "Connection Failed. The Bluetooth device might not be turned on.", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_LONG).show();
                isConnected = true;
            }
            progress.dismiss();
        }
    }
}
