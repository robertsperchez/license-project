package com.example.smartcart;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.widget.ListView;
import android.os.Bundle;

import java.util.Set;
import java.util.ArrayList;

import android.view.View;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.TextView;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

public class DeviceList extends AppCompatActivity {

    //Widgets
    Button showDevicesButton;
    ListView devicesList;
    //Bluetooth
    private BluetoothAdapter bluetoothAdapter = null;
    private Set<BluetoothDevice> pairedDevices;
    public static String EXTRA_ADDRESS = "device_address";

    /**
     * Main class, where the Activity is started, and we initialize the widgets. Also checks the device's bluetooth
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list);

        //Calling the widgets using their component id
        showDevicesButton = (Button) findViewById(R.id.showDevicesButton);
        devicesList = (ListView) findViewById(R.id.devicesList);

        //Gets the device's Bluetooth adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            //Shows a message if the device doesn't have Bluetooth
            Toast.makeText(getApplicationContext(), "The device's Bluetooth is not available!", Toast.LENGTH_LONG).show();
            finish();
        } else if (!bluetoothAdapter.isEnabled()) {
            //Asks the user to turn the Bluetooth on
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon, 1);
        }

        //Connects the function to the button, so that when pressed it will show the paired devices
        showDevicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectedDevicesList();
            }
        });
    }

    /**
     * This function gets all the paired devices of the smartphone, then it shows them on the ListView of the main page.
     */
    private void connectedDevicesList() {
        //Gets all the paired devices of the smartphone
        pairedDevices = bluetoothAdapter.getBondedDevices();
        ArrayList listToShow = new ArrayList();

        //Adds the paired devices to a list
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice bt : pairedDevices) {
                //Get the device's name and the address
                listToShow.add(bt.getName() + "\nAddress: " + bt.getAddress());
            }
        } else {
            Toast.makeText(getApplicationContext(), "No paired Bluetooth devices found for this smartphone.", Toast.LENGTH_LONG).show();
        }

        //Shows the list of paired devices
        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listToShow);
        devicesList.setAdapter(adapter);
        //Method called when the device from the listToShow is clicked
        devicesList.setOnItemClickListener(myListClickListener);
    }

    /**
     * Function is called whenever a paired device from the ListView is clicked. It then gets the info of the device we want to connect to,
     * and it starts the next activity
     */
    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick (AdapterView<?> adapterView, View view, int arg2, long arg3)
        {
            //Gets the device's MAC address, the last 17 chars in the View
            String info = ((TextView) view).getText().toString();
            String address = info.substring(info.length() - 17);

            //Makes an intent to start next activity
            Intent intent = new Intent(DeviceList.this, OptionsMenu.class);

            //Changes the activity
            intent.putExtra(EXTRA_ADDRESS, address); //The address string will be received in the next activities
            startActivity(intent);
        }
    };
}
